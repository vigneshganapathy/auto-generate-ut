import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest0 {

    public static boolean debug = false;

    @Test
    public void test1() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test1");
        java.lang.Object obj0 = new java.lang.Object();
        java.lang.Class<?> wildcardClass1 = obj0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
    }

    @Test
    public void test2() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test2");
        MapAdditionalDataModelToAdditionalData mapAdditionalDataModelToAdditionalData0 = new MapAdditionalDataModelToAdditionalData();
        java.lang.Class<?> wildcardClass1 = mapAdditionalDataModelToAdditionalData0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
    }

    @Test
    public void test3() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test3");
        MapAdditionalDataModelToAdditionalData mapAdditionalDataModelToAdditionalData0 = new MapAdditionalDataModelToAdditionalData();
        AdditionalDataModel[] additionalDataModelArray1 = new AdditionalDataModel[] {};
        java.util.ArrayList<AdditionalDataModel> additionalDataModelList2 = new java.util.ArrayList<AdditionalDataModel>();
        boolean boolean3 = java.util.Collections.addAll((java.util.Collection<AdditionalDataModel>) additionalDataModelList2, additionalDataModelArray1);
        java.util.List<AdditionalData> additionalDataList4 = mapAdditionalDataModelToAdditionalData0.map((java.util.List<AdditionalDataModel>) additionalDataModelList2);
        java.lang.Class<?> wildcardClass5 = additionalDataList4.getClass();
        org.junit.Assert.assertNotNull(additionalDataModelArray1);
        org.junit.Assert.assertTrue("'" + boolean3 + "' != '" + false + "'", boolean3 == false);
        org.junit.Assert.assertNotNull(additionalDataList4);
        org.junit.Assert.assertNotNull(wildcardClass5);
    }

    @Test
    public void test4() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test4");
        MapAdditionalDataModelToAdditionalData mapAdditionalDataModelToAdditionalData0 = new MapAdditionalDataModelToAdditionalData();
        MapAdditionalDataModelToAdditionalData mapAdditionalDataModelToAdditionalData1 = new MapAdditionalDataModelToAdditionalData();
        AdditionalDataModel[] additionalDataModelArray2 = new AdditionalDataModel[] {};
        java.util.ArrayList<AdditionalDataModel> additionalDataModelList3 = new java.util.ArrayList<AdditionalDataModel>();
        boolean boolean4 = java.util.Collections.addAll((java.util.Collection<AdditionalDataModel>) additionalDataModelList3, additionalDataModelArray2);
        java.util.List<AdditionalData> additionalDataList5 = mapAdditionalDataModelToAdditionalData1.map((java.util.List<AdditionalDataModel>) additionalDataModelList3);
        java.util.List<AdditionalData> additionalDataList6 = mapAdditionalDataModelToAdditionalData0.map((java.util.List<AdditionalDataModel>) additionalDataModelList3);
        MapAdditionalDataModelToAdditionalData mapAdditionalDataModelToAdditionalData7 = new MapAdditionalDataModelToAdditionalData();
        AdditionalDataModel[] additionalDataModelArray8 = new AdditionalDataModel[] {};
        java.util.ArrayList<AdditionalDataModel> additionalDataModelList9 = new java.util.ArrayList<AdditionalDataModel>();
        boolean boolean10 = java.util.Collections.addAll((java.util.Collection<AdditionalDataModel>) additionalDataModelList9, additionalDataModelArray8);
        java.util.List<AdditionalData> additionalDataList11 = mapAdditionalDataModelToAdditionalData7.map((java.util.List<AdditionalDataModel>) additionalDataModelList9);
        java.util.List<AdditionalData> additionalDataList12 = mapAdditionalDataModelToAdditionalData0.map((java.util.List<AdditionalDataModel>) additionalDataModelList9);
        java.util.List<AdditionalDataModel> additionalDataModelList13 = null;
        // The following exception was thrown during execution in test generation
        try {
            java.util.List<AdditionalData> additionalDataList14 = mapAdditionalDataModelToAdditionalData0.map(additionalDataModelList13);
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException; message: null");
        } catch (java.lang.NullPointerException e) {
            // Expected exception.
        }
        org.junit.Assert.assertNotNull(additionalDataModelArray2);
        org.junit.Assert.assertTrue("'" + boolean4 + "' != '" + false + "'", boolean4 == false);
        org.junit.Assert.assertNotNull(additionalDataList5);
        org.junit.Assert.assertNotNull(additionalDataList6);
        org.junit.Assert.assertNotNull(additionalDataModelArray8);
        org.junit.Assert.assertTrue("'" + boolean10 + "' != '" + false + "'", boolean10 == false);
        org.junit.Assert.assertNotNull(additionalDataList11);
        org.junit.Assert.assertNotNull(additionalDataList12);
    }

    @Test
    public void test5() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test5");
        MapAdditionalDataModelToAdditionalData mapAdditionalDataModelToAdditionalData0 = new MapAdditionalDataModelToAdditionalData();
        AdditionalDataModel[] additionalDataModelArray1 = new AdditionalDataModel[] {};
        java.util.ArrayList<AdditionalDataModel> additionalDataModelList2 = new java.util.ArrayList<AdditionalDataModel>();
        boolean boolean3 = java.util.Collections.addAll((java.util.Collection<AdditionalDataModel>) additionalDataModelList2, additionalDataModelArray1);
        java.util.List<AdditionalData> additionalDataList4 = mapAdditionalDataModelToAdditionalData0.map((java.util.List<AdditionalDataModel>) additionalDataModelList2);
        java.lang.Class<?> wildcardClass5 = mapAdditionalDataModelToAdditionalData0.getClass();
        org.junit.Assert.assertNotNull(additionalDataModelArray1);
        org.junit.Assert.assertTrue("'" + boolean3 + "' != '" + false + "'", boolean3 == false);
        org.junit.Assert.assertNotNull(additionalDataList4);
        org.junit.Assert.assertNotNull(wildcardClass5);
    }

    @Test
    public void test6() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test6");
        MapAdditionalDataModelToAdditionalData mapAdditionalDataModelToAdditionalData0 = new MapAdditionalDataModelToAdditionalData();
        MapAdditionalDataModelToAdditionalData mapAdditionalDataModelToAdditionalData1 = new MapAdditionalDataModelToAdditionalData();
        AdditionalDataModel[] additionalDataModelArray2 = new AdditionalDataModel[] {};
        java.util.ArrayList<AdditionalDataModel> additionalDataModelList3 = new java.util.ArrayList<AdditionalDataModel>();
        boolean boolean4 = java.util.Collections.addAll((java.util.Collection<AdditionalDataModel>) additionalDataModelList3, additionalDataModelArray2);
        java.util.List<AdditionalData> additionalDataList5 = mapAdditionalDataModelToAdditionalData1.map((java.util.List<AdditionalDataModel>) additionalDataModelList3);
        java.util.List<AdditionalData> additionalDataList6 = mapAdditionalDataModelToAdditionalData0.map((java.util.List<AdditionalDataModel>) additionalDataModelList3);
        MapAdditionalDataModelToAdditionalData mapAdditionalDataModelToAdditionalData7 = new MapAdditionalDataModelToAdditionalData();
        AdditionalDataModel[] additionalDataModelArray8 = new AdditionalDataModel[] {};
        java.util.ArrayList<AdditionalDataModel> additionalDataModelList9 = new java.util.ArrayList<AdditionalDataModel>();
        boolean boolean10 = java.util.Collections.addAll((java.util.Collection<AdditionalDataModel>) additionalDataModelList9, additionalDataModelArray8);
        java.util.List<AdditionalData> additionalDataList11 = mapAdditionalDataModelToAdditionalData7.map((java.util.List<AdditionalDataModel>) additionalDataModelList9);
        java.util.List<AdditionalData> additionalDataList12 = mapAdditionalDataModelToAdditionalData0.map((java.util.List<AdditionalDataModel>) additionalDataModelList9);
        MapAdditionalDataModelToAdditionalData mapAdditionalDataModelToAdditionalData13 = new MapAdditionalDataModelToAdditionalData();
        MapAdditionalDataModelToAdditionalData mapAdditionalDataModelToAdditionalData14 = new MapAdditionalDataModelToAdditionalData();
        AdditionalDataModel[] additionalDataModelArray15 = new AdditionalDataModel[] {};
        java.util.ArrayList<AdditionalDataModel> additionalDataModelList16 = new java.util.ArrayList<AdditionalDataModel>();
        boolean boolean17 = java.util.Collections.addAll((java.util.Collection<AdditionalDataModel>) additionalDataModelList16, additionalDataModelArray15);
        java.util.List<AdditionalData> additionalDataList18 = mapAdditionalDataModelToAdditionalData14.map((java.util.List<AdditionalDataModel>) additionalDataModelList16);
        java.util.List<AdditionalData> additionalDataList19 = mapAdditionalDataModelToAdditionalData13.map((java.util.List<AdditionalDataModel>) additionalDataModelList16);
        MapAdditionalDataModelToAdditionalData mapAdditionalDataModelToAdditionalData20 = new MapAdditionalDataModelToAdditionalData();
        AdditionalDataModel[] additionalDataModelArray21 = new AdditionalDataModel[] {};
        java.util.ArrayList<AdditionalDataModel> additionalDataModelList22 = new java.util.ArrayList<AdditionalDataModel>();
        boolean boolean23 = java.util.Collections.addAll((java.util.Collection<AdditionalDataModel>) additionalDataModelList22, additionalDataModelArray21);
        java.util.List<AdditionalData> additionalDataList24 = mapAdditionalDataModelToAdditionalData20.map((java.util.List<AdditionalDataModel>) additionalDataModelList22);
        java.util.List<AdditionalData> additionalDataList25 = mapAdditionalDataModelToAdditionalData13.map((java.util.List<AdditionalDataModel>) additionalDataModelList22);
        java.util.List<AdditionalData> additionalDataList26 = mapAdditionalDataModelToAdditionalData0.map((java.util.List<AdditionalDataModel>) additionalDataModelList22);
        org.junit.Assert.assertNotNull(additionalDataModelArray2);
        org.junit.Assert.assertTrue("'" + boolean4 + "' != '" + false + "'", boolean4 == false);
        org.junit.Assert.assertNotNull(additionalDataList5);
        org.junit.Assert.assertNotNull(additionalDataList6);
        org.junit.Assert.assertNotNull(additionalDataModelArray8);
        org.junit.Assert.assertTrue("'" + boolean10 + "' != '" + false + "'", boolean10 == false);
        org.junit.Assert.assertNotNull(additionalDataList11);
        org.junit.Assert.assertNotNull(additionalDataList12);
        org.junit.Assert.assertNotNull(additionalDataModelArray15);
        org.junit.Assert.assertTrue("'" + boolean17 + "' != '" + false + "'", boolean17 == false);
        org.junit.Assert.assertNotNull(additionalDataList18);
        org.junit.Assert.assertNotNull(additionalDataList19);
        org.junit.Assert.assertNotNull(additionalDataModelArray21);
        org.junit.Assert.assertTrue("'" + boolean23 + "' != '" + false + "'", boolean23 == false);
        org.junit.Assert.assertNotNull(additionalDataList24);
        org.junit.Assert.assertNotNull(additionalDataList25);
        org.junit.Assert.assertNotNull(additionalDataList26);
    }
}

